using System;
using ITExpertDependencyInjection.Products;

namespace ITExpertDependencyInjection.Clients
{
    public class NintendoEntertainmentSystem
    {
        private INesVideoGame VideoGame;

        public NintendoEntertainmentSystem(INesVideoGame cartridge) => 
            VideoGame = cartridge;

        public void Start() => VideoGame?.Run();
    }
}