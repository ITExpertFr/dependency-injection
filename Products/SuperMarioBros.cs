using System;

namespace ITExpertDependencyInjection.Products
{
    public class SuperMarioBros : INesVideoGame
    {
        public void Run() => Console.WriteLine("Playing Super Mario Bros");
    }
}