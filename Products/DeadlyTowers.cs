using System;

namespace ITExpertDependencyInjection.Products
{
    public class DeadlyTowers : INesVideoGame
    {
        public void Run() => Console.WriteLine("Suffering on Deadly Towers");
    }
}