using System;

namespace ITExpertDependencyInjection.Products
{
    public class Contra : INesVideoGame
    {
        public void Run() => Console.WriteLine("Playing Contra");
    }
}