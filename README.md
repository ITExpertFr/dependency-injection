# ITExpert - Dependency Injection
Ceci est le code source utilisé pour mon article sur l'injection de dépendance (Dependency Injection Pattern).

[Lien vers l'article](https://itexpert.fr/blog/injection-dependance)

### Prérequis
- Installer les SDK de .NET 5

### Guide d'utilisation
1. Cloner le code source.
2. Ouvrir la racine du répertoire dans une invite de commandes.
3. Éxecuter la commande "dotnet run" pour lancer le programme.

