﻿using System;
using ITExpertDependencyInjection.Clients;
using ITExpertDependencyInjection.Products;

namespace ITExpertDependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            NintendoEntertainmentSystem nes;

            var superMarioBrosCartridge = new SuperMarioBros();
            nes = new NintendoEntertainmentSystem(superMarioBrosCartridge);
            nes.Start();    // Playing Super Mario Bros

            var contraCartridge = new Contra();
            nes = new NintendoEntertainmentSystem(contraCartridge);
            nes.Start();    // Playing Contra

            var deadlyTowersCartridge = new DeadlyTowers();
            nes = new NintendoEntertainmentSystem(deadlyTowersCartridge);
            nes.Start();    // Suffering on Deadly Towers

            nes = new NintendoEntertainmentSystem(null);
            nes.Start();
        }
    }
}
